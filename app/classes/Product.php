<?php

namespace App\classes;

abstract class Product
{
    private $itemNumber;
    private $name;
    private $price;
    private $brand;

    public function __construct(
        int $itemNumber,
        string $name,
        float $price,
        Brand $brand
    ) {
        $this->itemNumber = $itemNumber;
        $this->name = $name;
        $this->price = $price;
        $this->brand = $brand;
    }

    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getBrand()
    {
        return $this->brand;
    }
}
