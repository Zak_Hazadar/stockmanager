<?php

namespace App\classes;

use App\classes\Brand;

class Food extends Product
{
    private $weight;

    public function __construct(
        int $itemNumber,
        string $name,
        float $price,
        Brand $brand,
        int $weight
    ) {
        parent::__construct(
            $itemNumber,
            $name,
            $price,
            $brand
        );
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }
}
