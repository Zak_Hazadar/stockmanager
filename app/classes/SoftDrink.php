<?php

namespace App\classes;

use App\classes\Brand;

class SoftDrink extends Product
{
    private $type;

    public function __construct(
        int $itemNumber,
        string $name,
        float $price,
        Brand $brand,
        string $type
    ) {
        parent::__construct(
            $itemNumber,
            $name,
            $price,
            $brand
        );
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }
}
