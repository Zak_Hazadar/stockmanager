<?php

namespace App\classes;

use App\classes\Product;

class Warehouse
{
    private $name;
    private $address;
    private $capacity;
    private $stock;

    public function __construct(
        string $name,
        string $address,
        int $capacity
    ) {
        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;
        $this->stock = [];
    }

    public function addProduct(Product $product): bool
    {
        if ($this->hasSpace()) {
            $this->stock [] = $product;
            return true;
        } else {
            return false;
        }
    }

    public function getProductCountByItemNumber(int $itemNumber)
    {
        $counter = 0;
        foreach ($this->stock as $product) {
            if ($product->getItemNumber() === $itemNumber) {
                $counter++;
            }
        }
        return $counter;
    }

    public function removeProduct(int $itemNumber): bool
    {
        foreach ($this->stock as $key => $product) {
            if ($product->getItemNumber() === $itemNumber) {
                unset($this->stock[$key]);
                return true;
            }
        }

        return false;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function getName()
    {
        return $this->name;
    }

    private function hasSpace(): bool
    {
        return count($this->stock) < $this->capacity ? true : false;
    }

    public function getFreeSpace(): int
    {
        return $this->capacity - count($this->stock);
    }
}
