<?php

namespace App\classes;

use Exception;

class StockManager
{
    private $warehouses = [];

    public function addWarehouse(Warehouse $warehouse)
    {
        $this->warehouses [] = $warehouse;
    }

    public function addProductToWarehouse(Product $product, int $count): bool
    {
        $allCapacity = 0;
        foreach ($this->warehouses as $warehouse) {
            $allCapacity += $warehouse->getFreeSpace();
        }
        if ($allCapacity < $count) {
            throw new Exception('Not enough space!');
        }

        foreach ($this->warehouses as $warehouse) {
            $space = $warehouse->getFreeSpace();
            $store = min($count, $space);
            for ($i = 0; $i < $store; $i++) {
                $warehouse->addProduct($product);
            }
            $count -= $store;
            if ($count == 0) {
                return true;
            }
        }
    }

    public function removeProductFromWarehouse(int $itemNumber, int $count): bool
    {
        $existingCount = 0;
        foreach ($this->warehouses as $warehouse) {
            $existingCount += $warehouse->getProductCountByItemNumber($itemNumber);
        }
        if ($existingCount < $count) {
            throw new Exception('Does not have enough product!');
        }

        foreach ($this->warehouses as $warehouse) {
            $available = $warehouse->getProductCountByItemNumber($itemNumber);

            $remove = min($count, $available);
            for ($i = 0; $i < $remove; $i++) {
                $warehouse->removeProduct($itemNumber);
            }
            $count -= $remove;
            if ($count == 0) {
                return true;
            }
        }
    }

    public function getWarehouseStocks(): array
    {
        $stocks = [];
        foreach ($this->warehouses as $warehouse) {
            $products = [];
            foreach ($warehouse->getStock() as $product) {
                $products [] = $product->getName();
            }
            $stocks [] = [
                'name' => $warehouse->getName(), 'stock' => $products
            ];
        }
        return $stocks;
    }
}
