<?php

namespace App\classes;

class Brand
{
    private $brandName;
    private $quality;

    public function __construct(string $brandName, int $quality)
    {
        $this->brandName = $brandName;
        $this->quality = $quality;
    }

    public function getBrandName()
    {
        return $this->brandName;
    }

    public function getQuality()
    {
        return $this->quality;
    }
}
