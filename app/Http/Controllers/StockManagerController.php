<?php

namespace App\Http\Controllers;

use App\classes\Brand;
use App\classes\Food;
use App\classes\SoftDrink;
use App\classes\StockManager;
use App\classes\Warehouse;
use Exception;
use Illuminate\Routing\Controller as BaseController;

class StockManagerController extends BaseController
{
    public function index()
    {
        $logs = [];
        $logs ['test1'] = $this->test1();
        $logs ['test2'] = $this->test2();
        $logs ['test3'] = $this->test3();
        $logs ['test4'] = $this->test4();
        $logs ['test5'] = $this->test5();
        $logs ['test6'] = $this->test6();
        $logs ['test7'] = $this->test7();
        $logs ['test8'] = $this->test8();

        dump($logs);
        return;
    }

    private function test1()
    {
        $stockManager = new StockManager();
        $warehouse1 = new Warehouse('Egyes raktár', 'Veszprém Valami ut', 30);
        $warehouse2 = new Warehouse('Kettes raktár', 'Budapest Valami ut', 20);
        $cocaCola = new Brand('CocaCola', 4);
        $sprite = new SoftDrink(3421211, 'Sprite', 299, $cocaCola, 'Dobozos');

        $stockManager->addWarehouse($warehouse1);
        $stockManager->addWarehouse($warehouse2);

        try {
            $stockManager->addProductToWarehouse($sprite, 40);
        } catch (Exception $e) {
            return 'FAILED';
        }

        return 'SUCCESS';
    }

    private function test2()
    {
        $stockManager = new StockManager();
        $warehouse1 = new Warehouse('Egyes raktár', 'Veszprém Valami ut', 30);
        $warehouse2 = new Warehouse('Kettes raktár', 'Budapest Valami ut', 20);
        $cocaCola = new Brand('CocaCola', 4);
        $sprite = new SoftDrink(3421211, 'Sprite', 299, $cocaCola, 'Dobozos');

        $stockManager->addWarehouse($warehouse1);
        $stockManager->addWarehouse($warehouse2);

        try {
            $stockManager->addProductToWarehouse($sprite, 140);
        } catch (Exception $e) {
            return 'SUCCESS';
        }

        return 'FAILED';
    }

    private function test3()
    {
        $stockManager = new StockManager();
        $warehouse1 = new Warehouse('Egyes raktár', 'Veszprém Valami ut', 30);
        $warehouse2 = new Warehouse('Kettes raktár', 'Budapest Valami ut', 20);
        $cocaCola = new Brand('CocaCola', 4);
        $sprite = new SoftDrink(3421211, 'Sprite', 299, $cocaCola, 'Dobozos');

        $stockManager->addWarehouse($warehouse1);
        $stockManager->addWarehouse($warehouse2);

        try {
            $stockManager->addProductToWarehouse($sprite, 40);
        } catch (Exception $e) {
            return 'FAILED';
        }

        try {
            $stockManager->removeProductFromWarehouse($sprite->getItemNumber(), 35);
        } catch (Exception $e) {
            return 'FAILED';
        }

        return 'SUCCESS';
    }

    private function test4()
    {
        $stockManager = new StockManager();
        $warehouse1 = new Warehouse('Egyes raktár', 'Veszprém Valami ut', 30);
        $warehouse2 = new Warehouse('Kettes raktár', 'Budapest Valami ut', 20);
        $cocaCola = new Brand('CocaCola', 4);
        $sprite = new SoftDrink(3421211, 'Sprite', 299, $cocaCola, 'Dobozos');

        $stockManager->addWarehouse($warehouse1);
        $stockManager->addWarehouse($warehouse2);

        try {
            $stockManager->addProductToWarehouse($sprite, 40);
        } catch (Exception $e) {
            return 'FAILED';
        }

        try {
            $stockManager->removeProductFromWarehouse($sprite->getItemNumber(), 135);
        } catch (Exception $e) {
            return 'SUCCESS';
        }

        return 'FAILED';
    }

    private function test5()
    {
        $stockManager = new StockManager();
        $warehouse1 = new Warehouse('Egyes raktár', 'Veszprém Valami ut', 30);
        $warehouse2 = new Warehouse('Kettes raktár', 'Budapest Valami ut', 20);
        $cocaCola = new Brand('CocaCola', 4);
        $sprite = new SoftDrink(3421211, 'Sprite', 299, $cocaCola, 'Dobozos');
        $fanta = new SoftDrink(3421212, 'Fanta', 199, $cocaCola, 'Orange');

        $stockManager->addWarehouse($warehouse1);
        $stockManager->addWarehouse($warehouse2);

        try {
            $stockManager->addProductToWarehouse($sprite, 40);
            $stockManager->removeProductFromWarehouse($sprite->getItemNumber(), 35);
            $stockManager->addProductToWarehouse($sprite, 20);
            $stockManager->removeProductFromWarehouse($sprite->getItemNumber(), 15);
            $stockManager->addProductToWarehouse($fanta, 40);
        } catch (Exception $e) {
            return 'FAILED';
        }

        try {
            $stockManager->removeProductFromWarehouse($fanta->getItemNumber(), 35);
        } catch (Exception $e) {
            return 'FAILED';
        }

        return 'SUCCESS';
    }

    private function test6()
    {
        $stockManager = new StockManager();
        $warehouse1 = new Warehouse('Egyes raktár', 'Veszprém Valami ut', 30);
        $warehouse2 = new Warehouse('Kettes raktár', 'Budapest Valami ut', 20);
        $cocaCola = new Brand('CocaCola', 4);
        $sprite = new SoftDrink(3421211, 'Sprite', 299, $cocaCola, 'Dobozos');

        $stockManager->addWarehouse($warehouse1);
        $stockManager->addWarehouse($warehouse2);

        try {
            $stockManager->addProductToWarehouse($sprite, 40);
            $stockManager->removeProductFromWarehouse($sprite->getItemNumber(), 15);
            $stockManager->removeProductFromWarehouse($sprite->getItemNumber(), 15);
        } catch (Exception $e) {
            return 'FAILED';
        }

        try {
            $stockManager->removeProductFromWarehouse($sprite->getItemNumber(), 15);
        } catch (Exception $e) {
            return 'SUCCESS';
        }

        return 'FAILED';
    }

    private function test7()
    {
        $stockManager = new StockManager();
        $warehouse1 = new Warehouse('Egyes raktár', 'Veszprém Valami ut', 30);
        $warehouse2 = new Warehouse('Kettes raktár', 'Budapest Valami ut', 20);
        $cocaCola = new Brand('CocaCola', 4);
        $lays = new Brand('Lays', 3);
        $sprite = new SoftDrink(3421211, 'Sprite', 299, $cocaCola, 'Dobozos');
        $OnionChips = new Food(3421213, 'OnionChips', 149, $lays, 90);

        $stockManager->addWarehouse($warehouse1);
        $stockManager->addWarehouse($warehouse2);

        try {
            $stockManager->addProductToWarehouse($sprite, 40);
            $stockManager->addProductToWarehouse($OnionChips, 5);
        } catch (Exception $e) {
            return 'FAILED';
        }

        return 'SUCCESS';
    }

    private function test8()
    {
        $stockManager = new StockManager();
        $warehouse1 = new Warehouse('Egyes raktár', 'Veszprém Valami ut', 30);
        $warehouse2 = new Warehouse('Kettes raktár', 'Budapest Valami ut', 20);
        $warehouse3 = new Warehouse('Kettes raktár', 'Budapest Valamimas ut', 15);
        $cocaCola = new Brand('CocaCola', 4);
        $lays = new Brand('Lays', 3);
        $sprite = new SoftDrink(3421211, 'Sprite', 299, $cocaCola, 'Dobozos');
        $fanta = new SoftDrink(3421212, 'Fanta', 299, $cocaCola, 'Narancs');
        $onion = new Food(3421213, 'Onion', 299, $lays, 70);

        $stockManager->addWarehouse($warehouse1);
        $stockManager->addWarehouse($warehouse2);
        $stockManager->addWarehouse($warehouse3);

        try {
            $stockManager->addProductToWarehouse($sprite, 20);
            $stockManager->addProductToWarehouse($fanta, 15);
            $stockManager->addProductToWarehouse($sprite, 10);
            $stockManager->addProductToWarehouse($onion, 8);
            $stockManager->addProductToWarehouse($onion, 11);
        } catch (Exception $e) {
            return 'FAILED';
        }

        return $stockManager->getWarehouseStocks();
    }
}
